#pragma once
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\System\Vector3.hpp>
#include "Globals.h"
#include "Level.h"
#include "CommandChain.h"

class Player
{
public:
	Player(Level* p_level);
	~Player();
	void update();
	void setPosition(sf::Vector3f p_pos);
	void setQueue(COMMAND p_command, LANE p_lane);
	void refreshQueue();
	bool attack(LANE p_lane);
	//Test functions
	sf::Sprite getSprite();
	//
	sf::Vector3f getPosition();

private:
	Level* level;
	Queue queue[2];
	//Test variables
	sf::Texture texture_;
	sf::Sprite sprite_;
	//
	sf::Vector3f pos_;
	
};