#pragma once
#include "Globals.h"
#include "IEntity.h"
#include "CommandChain.h"

class Victim : public IEntity
{
public:
	Victim();
	~Victim();
	void update();
	void setHealth(float p_amount);
	
	bool attacking();
	bool blocking();
	bool shooting();

	float getHealth();

private:
	float health_;
	COMMAND state_;

};
