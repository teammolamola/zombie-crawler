#pragma once
#include <SFML\System.hpp>

inline float clamp(float p_a, float p_b, float p_c)
{
	if (p_a < p_b)
		return p_b;
	if (p_a > p_c)
		return p_c;
	else
		return p_a;
}

inline sf::Vector2f vectorDifference(sf::Vector2f p_a, sf::Vector2f p_b)
{
	
	return p_a - p_b;
}