#pragma once
#include <vector>
#include <stack>
#include <SFML\System.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

#include "Globals.h"
#include "IEntity.h"


class Lane
{
public:
	
	Lane();
	~Lane();
	void add(IEntity* p_entity); 
	void attack(float p_damage);
	sf::RectangleShape getHitBox();
	IEntity* getEntity();
private:
	std::stack<IEntity*> entities;
	//bool occupied_;

	sf::RectangleShape hitBox_;
};