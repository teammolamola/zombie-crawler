#include "Level.h"

Level::Level()
{
	for (int i = 0; i < 3; i++)
	{
		lanes[i] = new Lane;
	}
}

Level::~Level()
{
	for (int i = 0; i < 3; i++)
	{
		delete lanes[i];
	}
}

void Level::update()
{

}

Lane* Level::getLane(LANE p_lane)
{
	return lanes[p_lane];
}
