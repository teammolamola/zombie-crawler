#pragma once
#include <vector>
#include "Globals.h"
#include "Command.h"

class CommandChain
{
public:
	CommandChain();
	~CommandChain();
	void run(float deltatime);
	void add(Command* p_command);

private:
	std::vector<Command*> commands;
	COMMAND lastCommand;

	//Test
	sf::Clock clock;
	float frameA;
	float frameB;
	//Test

};