#pragma once

class IEntity
{
public:
	virtual ~IEntity() {};
	virtual void update() = 0;
	virtual void setHealth(float p_amount) = 0;
	virtual float getHealth() = 0;

private:
	
};