#include "Lane.h"

Lane::Lane()
{
}

Lane::~Lane()
{
}

void Lane::add(IEntity* p_entity)
{
	entities.push(p_entity);
}

void Lane::attack(float p_damage)
{
	if (!entities.empty())
	{
		entities.top()->setHealth(p_damage);
		if (entities.top()->getHealth() <= 0)
		{
			entities.pop();
		}
	}
}

sf::RectangleShape Lane::getHitBox()
{
	return sf::RectangleShape();
}

IEntity* Lane::getEntity()
{
	return entities.top();
}
