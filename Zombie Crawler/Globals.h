#pragma once

enum LANE
{
	LANELEFT,
	LANEMIDDLE,
	LANERIGHT
};

enum OBJECT
{
	OBSTACLE,
	CRITTER,
	VICTIM
};

enum COMMAND
{
	IDLE,
	ATTACK,
	DODGE,
	MOVE,
	BLOCK,
	SHOOT

};

struct Queue
{
	bool active_ = false;
	COMMAND command_;
	LANE lane_;
};