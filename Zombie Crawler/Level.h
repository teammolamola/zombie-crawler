#pragma once
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

#include "Globals.h"
#include "Lane.h"

class Level
{
public:
	Level();
	~Level();
	void update();
	void clearLane();
	Lane* getLane(LANE p_lane);
	
private:
	Lane* lanes[3];

};