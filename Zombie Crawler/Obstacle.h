#pragma once
#include <SFML\Graphics.hpp>
#include "IEntity.h"

class Obstacle : public IEntity
{
public:
	Obstacle(sf::Vector2f p_pos /*sf::Vector3f*/);
	~Obstacle();
	void update();
	void setHealth(float p_amount);
	
	float getHealth();
	sf::Sprite getSprite();

private:
	//Test - Sprite
	sf::Sprite sprite;
	sf::Texture texture;
	//
	float health_;
	
};