#pragma once
#include <SFML\System\Clock.hpp>
#include "Globals.h"

class Command
{
public:
	virtual ~Command() {};
	virtual void execute() = 0;
	virtual void undo() = 0;
	virtual COMMAND getType() = 0;
	virtual sf::Time getTimer() = 0;

private:

};
