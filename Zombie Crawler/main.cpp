#define GLEW_STATIC

#include <glew.h>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>

#include <iostream>

#include "Globals.h"
#include "CommandChain.h"
#include "AttackCommand.h"
#include "Level.h"
#include "Lane.h"
#include "Player.h"
#include "Obstacle.h"

int main()
{
	sf::RenderWindow window(sf::VideoMode(1200, 900, 32), "OpenGL Test", sf::Style::Titlebar | sf::Style::Close);
	//
	GLenum glewFail = glewInit();
	if (glewFail != GLEW_OK)
	{
		printf("failed to load Glew");
		return 1;
	}
	GLuint vertexBuffer;
	glGenBuffers(1, &vertexBuffer);
	glEnable(GL_TEXTURE_2D);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0.0, 1200.0, 900.0, 0.0, -1.0, 10.0);
	//Test  

	Obstacle* obstacle = new Obstacle(sf::Vector2f(200.0f, 450.0f));
	Obstacle* obstacle2 = new Obstacle(sf::Vector2f(600.0f, 450.0f));
	Obstacle* obstacle3 = new Obstacle(sf::Vector2f(900.0f, 400.0f));
	Obstacle* obstacle4 = new Obstacle(sf::Vector2f(900.0f, 450.0f));
	Obstacle* obstacle5 = new Obstacle(sf::Vector2f(900.0f, 500.0f));
	Level* level = new Level;
	level->getLane(LANELEFT)->add(obstacle);
	level->getLane(LANEMIDDLE)->add(obstacle2);
	level->getLane(LANERIGHT)->add(obstacle3);
	level->getLane(LANERIGHT)->add(obstacle4);
	level->getLane(LANERIGHT)->add(obstacle5);
	CommandChain commandChain;
	Player* player = new Player(level);
	sf::Texture objTex;
	objTex.loadFromFile("../assets/wall_cat.png");
	sf::Sprite obj;
	obj.setTexture(objTex);
	obj.setOrigin(objTex.getSize().x / 2, objTex.getSize().y / 2);
	obj.setPosition(600.0f, 450.0f);


	//
	sf::Event events;
	bool running = true;
	while (running)
	{
		while (window.pollEvent(events))
		{
			if (events.type == sf::Event::Closed)
			{
				running = false;
			}
			sf::Clock clock;
			commandChain.run(0.1f);
			player->update();
			//Test - Input
			if (events.type == sf::Event::KeyPressed)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
				{
					obj.scale(1.03f, 1.03f);
					printf("Forward");
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
				{
					AttackCommand* attack = new AttackCommand(player, LANELEFT);
					commandChain.add(attack);
					printf("Left attack");
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
				{
					AttackCommand* attack = new AttackCommand(player, LANEMIDDLE);
					commandChain.add(attack);
					printf("Middle attack");
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
				{
					AttackCommand* attack = new AttackCommand(player, LANERIGHT);
					commandChain.add(attack);
					printf("Right attack");
				}
			}
			//
		}
		//Test - Clear/draw
		window.clear(sf::Color::White);
		window.draw(obj);
		if(obstacle->getHealth() > 0)
			window.draw(obstacle->getSprite());
		if (obstacle2->getHealth() > 0)
			window.draw(obstacle2->getSprite());
		if (obstacle3->getHealth() > 0)
			window.draw(obstacle3->getSprite());
		if (obstacle4->getHealth() > 0)
			window.draw(obstacle4->getSprite());
		if (obstacle5->getHealth() > 0)
			window.draw(obstacle5->getSprite());
		window.draw(player->getSprite());
		//
		window.display();
	}
}