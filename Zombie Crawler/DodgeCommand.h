#pragma once
#include "Globals.h"
#include "Command.h"

class DodgeCommand : public Command
{
public:
	DodgeCommand();
	~DodgeCommand();
	void execute();
	void undo();
	COMMAND getType();
	sf::Time getTimer();

private:
	sf::Time timer;
};