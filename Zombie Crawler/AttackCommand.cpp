#include "AttackCommand.h"
#include "Globals.h"

AttackCommand::AttackCommand(Player* p_player, LANE p_lane)
{
	player = p_player;
	lane_ = p_lane;
	timer_ = sf::seconds(1);
}

AttackCommand::~AttackCommand()
{
}

void AttackCommand::execute()
{
	player->attack(lane_);
}

void AttackCommand::undo()
{

}

COMMAND AttackCommand::getType()
{
	return ATTACK;
}

sf::Time AttackCommand::getTimer()
{
	return timer_;
}
