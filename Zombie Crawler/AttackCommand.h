#pragma once
#include "Command.h"
#include "Player.h"

class AttackCommand : public Command
{
public:
	AttackCommand(Player* p_player, LANE p_lane);
	~AttackCommand();
	void execute();
	void undo();
	
	COMMAND getType();
	sf::Time getTimer();

private:
	Player* player;
	LANE lane_;

	sf::Time timer_;
};