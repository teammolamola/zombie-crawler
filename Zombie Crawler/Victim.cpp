#include "Victim.h"

Victim::Victim()
{
	
}
                                         
Victim::~Victim()
{
}

void Victim::update()
{
	switch (state_)
	{
	case IDLE:
		
		break;
	case ATTACK:
		attacking();
		break;
	case BLOCK:
		blocking();
		break;
	case SHOOT:
		shooting();
		break;
	default:
		break;
	}


}

void Victim::setHealth(float p_amount)
{
	health_ += p_amount;
}

bool Victim::attacking()
{
	//Attack related code
	//Timing check

	if (/*attack is executed*/ false)
	{

		return true;
	}
	return false;
}

bool Victim::blocking()
{
	return false;
}

bool Victim::shooting()
{
	return false;
}

float Victim::getHealth()
{
	return health_;
}
