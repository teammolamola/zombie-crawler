#pragma once
#include <glew.h>

class Shaders
{
public:
	Shaders(const GLchar* vertexSource, const GLchar* fragmentSource);
	~Shaders();
	void useShader();
	void closeShader();
	GLuint getProgram();

private:

	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint shaderProgram;

};