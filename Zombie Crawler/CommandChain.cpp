#include "CommandChain.h"
#include <iostream>

CommandChain::CommandChain()
{
}

CommandChain::~CommandChain()
{
	for (int i = 0; i < commands.size();)
	{
		delete commands.at(i);
	}
}

void CommandChain::run(float deltatime)
{
	/*if (clock.getElapsedTime().asSeconds() > 1.0f)
	{
		clock.restart();
	}*/
	if (!commands.empty())
	{
		auto it = commands.begin();
		lastCommand = (*it)->getType();
		(*it)->execute();
		/*if ((*it)->getType() == ATTACK)
		{
			frameA = clock.getElapsedTime().asMicroseconds();
			std::cout << frameA << std::endl;;
		}
		else if ((*it)->getType() == BLOCK)
		{
			frameB = clock.getElapsedTime().asMicroseconds();
			std::cout << frameB << std::endl;
		}*/
		delete (*it);
		commands.erase(it);
		return;
	}
}

void CommandChain::add(Command* p_command)
{
	commands.push_back(p_command);
}

