#include <iostream>
#include "Player.h"

Player::Player(Level* p_level)  //Level pointer //Input pointer //commandchain
{
	//Test - Sprite
	texture_.loadFromFile("../assets/normal_hand.png");
	sprite_.setTexture(texture_);
	sprite_.setOrigin(texture_.getSize().x / 2, texture_.getSize().y / 2);
	sprite_.setPosition(1200.0f - texture_.getSize().x / 2, 900.0f - texture_.getSize().y / 2);
	sprite_.rotate(45.0f);
	//
	level = p_level;

}

Player::~Player()
{
}

void Player::update()
{
	refreshQueue();
	//set texture to position
}

void Player::setPosition(sf::Vector3f p_pos)
{
	pos_ = p_pos;
}

void Player::setQueue(COMMAND p_command, LANE p_lane)
{
	for (int i = 0; i < 2; i++)
	{
		if (!queue[i].active_)
		{
			queue[i].active_ = true;
			queue[i].command_ = p_command;
			queue[i].lane_ = p_lane;
		}
	}
}

void Player::refreshQueue()
{
	if (queue[0].active_)
	{
		switch (queue[0].command_)
		{
		case ATTACK:
			if (!attack(queue[0].lane_))
			{
				queue[0].active_ = false;
			}
			break;
		default:
			break;
		}

	}
	if (!queue[0].active_ && queue[1].active_)
	{
		queue[0] = queue[1];
		queue[1].active_ = false;
	}
}

bool Player::attack(LANE p_lane)
{
	switch (p_lane)
	{
	case LANELEFT:
		std::cout << "ATTACKING LANE LEFT" << std::endl;
		level->getLane(LANELEFT)->attack(-1.0f);
		return false;
		break;
	case LANEMIDDLE:
		std::cout << "ATTACKING LANE MIDDLE" << std::endl;
		level->getLane(LANEMIDDLE)->attack(-1.0f);
		return false;
		break;
	case LANERIGHT:
		std::cout << "ATTACKING LANE RIGHT" << std::endl;
		level->getLane(LANERIGHT)->attack(-1.0f);
		return false;
		break;
	default:
		break;
	}
	
	return true;
}


sf::Sprite Player::getSprite()
{
	return sprite_;
}

sf::Vector3f Player::getPosition()
{
	return pos_;
}